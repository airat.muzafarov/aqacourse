SELECT goods.name, prices.value
FROM goods, prices
WHERE goods.id = prices.id AND prices.value IN (SELECT MAX(value) FROM prices)

SELECT g.name, p.value
FROM goods g join prices p on g.id = p.goods_id
JOIN quantity q on g.id = q.goods_id
WHERE q.value IN (SELECT MIN(quantity.value) FROM quantity)

SELECT AVG(p.value) as val, m.name
FROM prices p JOIN goods g on p.goods_id = g.id
JOIN suppliers s on g.supplier_id = s.id
JOIN manufacturer m on m.id = s.manufacturer_id
GROUP BY supplier_id, m.name
ORDER BY val DESC
limit 1

SELECT goods.name, p.value, m.name
FROM goods JOIN manufacturer m on goods.id = m.id
JOIN prices p on goods.id = p.id
WHERE m.location = 'Moscow'