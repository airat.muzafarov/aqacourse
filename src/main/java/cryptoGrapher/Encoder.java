package cryptoGrapher;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import java.security.Key;

public class Encoder {

    public byte[] encode(String text, Key key, IvParameterSpec ivSpec) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
        byte[] encodeText = cipher.doFinal(text.getBytes());
 //       System.out.println(DatatypeConverter.printHexBinary(encodeText));
        return encodeText;
    }
}
