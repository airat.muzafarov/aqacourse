package cryptoGrapher;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import java.security.Key;

public class Decoder {

    public String decode(byte[] enc, Key key, IvParameterSpec ivSpec) throws Exception {
        String transformation = "AES/CBC/PKCS5Padding";
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
        return new String(cipher.doFinal(enc));
    }
}
