package creditor;

public class Human {
    private String name;
    private int age;
    private int sum;

    public Human(String name, int age, int sum) {
        this.name = name;
        this.age = age;
        this.sum = sum;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getSum() {
        return sum;
    }
}
