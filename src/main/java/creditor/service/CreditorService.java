package creditor.service;

import creditor.model.DeleteResponse;
import creditor.model.LoanRequest;
import creditor.repository.LoanRequestRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CreditorService  {
    private LoanRequestRepository repository;

    public Integer createLoan(LoanRequest loanRequest) {
        LoanRequest loan = repository.save(loanRequest);
        return loan.getId();
    }

    public LoanRequest getLoanById(Integer id) {
        return repository.findById(id).get();
    }

    public DeleteResponse deleteLoanById(Integer id) {
        repository.deleteById(id);
        return new DeleteResponse(true);
    }

    public List<LoanRequest> getLoanList() {
        return repository.findAll();
    }
}

