package creditor.controller;

import creditor.model.DeleteResponse;
import creditor.model.LoanRequest;
import creditor.service.CreditorService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/creditor")
@AllArgsConstructor
public class CreditorRestController {
    public final CreditorService creditorService;

    @PostMapping(path = "/save", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Integer createLoan(@RequestBody LoanRequest loanRequest) {
        return creditorService.createLoan(loanRequest);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public LoanRequest getLoan(@PathVariable Integer id) {
        return creditorService.getLoanById(id);
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DeleteResponse deleteCar(@PathVariable("id") Integer id) {
        return creditorService.deleteLoanById(id);
    }

    @GetMapping
    public List<LoanRequest> getList() {
        return creditorService.getLoanList();
    }
}
