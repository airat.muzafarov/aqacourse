package creditor;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class Credit {

    public String getJsonFromList(List<?> list) {
        Type type = new TypeToken<List<?>>(){}.getType();
        return new Gson().toJson(list, type);
    }

    public void checkHumansForCredit(String humanList) {
        Type type = new TypeToken<List<Human>>(){}.getType();
        List<Human> list = new Gson().fromJson(humanList, type);
        for (Human human : list) {
            boolean result = checkHumanForCredit(human);
            System.out.println("Кредит " + human.getName() + " выдан: " + result);
        }
    }

    public boolean checkHumanForCredit(Human human) {
            return human.getAge() >= 18 && !human.getName().equals("Bob") && human.getSum() <= human.getAge() * 100;
    }

    public static void main(String[] args) {
        Credit credit = new Credit();
        credit.checkHumansForCredit(credit.getJsonFromList(Arrays.asList(
                new Human("name10", 15, 2000),
                new Human("name1", 16, 3000),
                new Human("name2", 17, 1400),
                new Human("name4", 19, 2000),
                new Human("name5", 20, 1900),
                new Human("name3", 18, 1500),
                new Human("name6", 21, 2000),
                new Human("name7", 22, 3600),
                new Human("name8", 23, 4600),
                new Human("name9", 24, 1600))));

    }
}
