import java.util.Scanner;

public class CreditCalculator {

    public void calculate() {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите имя");
        String name = input.nextLine();
        System.out.println("Введите возраст");
        int age = input.nextInt();
        System.out.println("Введите сумму");
        int sum = input.nextInt();
        String message = (age >= 18 & !name.equals("Bob") & sum <= age * 100) ? "Кредит выдан" : "Кредит не выдан";
        System.out.println(message);
    }
}
