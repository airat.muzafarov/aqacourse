package phoneFinder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneFinder {
    private final String regEx = "(8(123|345|444|564|643|472|675|553|889|555)\\d{7})|" +
            "(8(1423|3456|4144|9564|4643|2345|2355|7896|1255|0098)\\d{6})";

    public String getString(long qty) {
        Random rnd = new Random();
        char[] numbers = "0123456789".toCharArray();
        String result = "";
        long i = 0;
        while (i < qty) {
            i++;
            result = result + numbers[rnd.nextInt(10)];
        }
        return result;
    }

    public List<String> getPhoneNumbers(String str) {
        List<String> result = new ArrayList<>();
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            result.add(matcher.group());
        }
        return result;
    }

    public static void main(String[] args) {
        PhoneFinder phoneFinder = new PhoneFinder();
        String str = phoneFinder.getString(50000);
        System.out.println(phoneFinder.getPhoneNumbers(str));
    }
}
