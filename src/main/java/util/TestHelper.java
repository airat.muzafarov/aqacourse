package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

public class TestHelper {
    private static final Random rnd = new Random();

    private TestHelper() {
    }

    public static int getRandomNumber() {
        return getNumber();
    }

    public static long getRandomLong() {
        return getRandom();
    }

    public static String generateString(int wordLength) {
        return getRandomString(wordLength);
    }

    public static HashMap<String, String> readFile(String filePath) throws IOException {
        return readFileToHashMap(filePath);
    }

    public static String dateFormatter(String dateString) {
        return format(dateString);
    }

    public static double stringFormatter(String str) {
        return formatStringToDouble(str);
    }

    private static String format(String dateString) {
        LocalDate date = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return date.format(DateTimeFormatter.ofPattern("dd MMMM yyyy", new Locale("ru")));
    }

    private static double formatStringToDouble(String str) {
        double result;
        try {
            result = Double.parseDouble(str);
        } catch (NumberFormatException n) {
            result = Double.NEGATIVE_INFINITY;

        }
        return result;
    }

    private static HashMap<String, String> readFileToHashMap(String filePath) throws IOException {
        HashMap<String, String> map = new HashMap<>();
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split(":", 2);
            if (parts.length >= 2) {
                String key = parts[0];
                String value = parts[1];
                map.put(key, value);
            } else {
                System.out.println("ignoring line: " + line);
            }
        }
        for (String key : map.keySet()) {
            System.out.println(key + ":" + map.get(key));
        }
        reader.close();
        return map;
    }

    private static String getRandomString(int wordLength) {
        StringBuilder result = new StringBuilder();
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        char[] lowerChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toLowerCase().toCharArray();
        for (int k = 0; k < 3; k++) {
            result.append(chars[rnd.nextInt(chars.length)]);
            for (int i = 0; i <= wordLength - 2; i++) {
                result.append(lowerChars[rnd.nextInt(lowerChars.length)]);
            }
            result.append(" ");
        }
        return result.toString().trim();
    }

    private static long getRandom() {
        return rnd.nextLong();
    }

    private static int getNumber() {
        int min = 1970;
        int max = LocalDate.now().getYear();
        int diff = max - min;
        return rnd.nextInt(diff + 1) + min;
    }
}
