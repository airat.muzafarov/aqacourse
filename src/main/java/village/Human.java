package village;

import java.util.Objects;

public class Human {

    private Role role;
    private String fullName;

    public Human(Role role, String fullName) {
        this.role = role;
        this.fullName = fullName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getRole() == human.getRole() && Objects.equals(getFullName(), human.getFullName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRole(), getFullName());
    }

    @Override
    public String toString() {
        return fullName + "(" + role + ")";
    }
}
