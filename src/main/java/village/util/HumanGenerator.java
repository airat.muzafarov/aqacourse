package village.util;

import village.Human;
import village.Role;

import java.util.*;

public class HumanGenerator {

    private static List<String> name = Arrays.asList("Dmitry", "Alexey", "Anton", "Aleksandr", "Petr", "Evgeniy", "Ivan", "Mikhail"
            , "Constantine", "Andrey");
    private static List<String> surname = Arrays.asList("Ivanov", "Petrov", "Sidorov", "Kuznetcov", "Fetisov", "Gagarin", "Yashin",
            "Navalny", "Putin", "Yakovlev");
    private static List<Role> roles = Arrays.asList(Role.WITCH, Role.WEREWOLF, Role.PEASANT, Role.VAMPIRE);
    public final static int MAX_VILLAGER_NUMBER = 50;

    public static List<Human> getPopulation(int populationLimit) {
        Random rnd = new Random();
        Set<Human> result = new HashSet<>();
        if (populationLimit > MAX_VILLAGER_NUMBER) {
            populationLimit = MAX_VILLAGER_NUMBER;
        }
        while (result.size() < populationLimit) {
            result.add(new Human(roles.get(rnd.nextInt(roles.size())), name.get(rnd.nextInt(name.size())) + " "
                    + surname.get(rnd.nextInt(surname.size()))));
        }

        return new ArrayList<>(result);
    }

}
