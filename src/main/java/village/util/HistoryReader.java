package village.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class HistoryReader {

    public void writeToFile(List<String> history) throws IOException {
        Path file = Paths.get("src/main/java/village/history/VillageHistory.txt");
            Files.write(file, history);
    }

    public void readFromFile(Path path) throws IOException {
        List<String> strings = Files.readAllLines(path);
        for (String str : strings) {
            System.out.println(str);
        }
    }
}
