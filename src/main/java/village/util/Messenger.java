package village.util;

import village.Human;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Messenger {

    private static List<String> history = new ArrayList<>();

    private static String getDate(Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_MONTH) + " " + new DateFormatSymbols().getShortMonths()[calendar.get(Calendar.MONTH)] +
                " " + calendar.get(Calendar.YEAR) + " (" + new DateFormatSymbols().getWeekdays()[calendar.get(Calendar.DAY_OF_WEEK)] + ") ";
    }

    public static String print(Calendar calendar) {
        String result = getDate(calendar) + "Все спят. Ничего не происходит";
        System.out.println(result);
        history.add(result);
        return result;
    }

    public static String printMessage(Human vl1, MessageType mType, Calendar calendar) {
        return printMessage(vl1, null, mType, calendar);
    }

    public static String printMessage(Human vl1, Human vl2, MessageType mType, Calendar calendar) {
        String result;
        String day = getDate(calendar);

        switch (mType) {
            case Leave:
                result = day + vl1.toString() + " left the Village.";
                System.out.println(result);
                history.add(result);
                break;
            case Appeared:
                result = day + vl1.toString() + " appeared the Village.";
                System.out.println(result);
                history.add(result);
                break;
            case Kill:
                result = day + "MURDEREXCEPTION " + vl2.toString() + " is killed by "
                        + vl1.toString();
                System.out.println(result);
                history.add(result);
                break;
            case KillVampire:
                result = day + "DAWNEXCEPTION " + vl1.toString() + " is killed by SunRise";
                System.out.println(result);
                history.add(result);
                break;
            case BecameVampire:
                result = day + vl1.toString() + " became a vampire";
                System.out.println(result);
                history.add(result);
                break;
            case Tea:
                result = day + vl1.toString() + " drinks tea with " + vl2.toString();
                System.out.println(result);
                history.add(result);
                break;
            default:
                result = "DEFAULT" + day + vl1.getFullName();
                System.out.println("DEFAULT" + day + vl1.getFullName());
                history.add(result);
        }
        return result;
    }

    public static List<String> getHistory() {
        return history;
    }
}

