package village.util;

import village.Human;

import java.util.Calendar;
import java.util.List;

public class MurderException extends  Exception{
    public MurderException() {
    }

    public MurderException(Human killer, Human victim, List<Human> lstVil, Calendar calendar){
        lstVil.remove(killer);
        Messenger.printMessage(killer,victim, MessageType.Kill, calendar);
    }
}

