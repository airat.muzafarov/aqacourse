package village.util;

import village.Human;

import java.util.Calendar;
import java.util.List;

public class DawnException extends  Exception{
    public DawnException() {
    }

    public DawnException(Human vampire, List<Human> lstVil, Calendar calendar){
        lstVil.remove(vampire);
        Messenger.printMessage(vampire, MessageType.KillVampire, calendar);
    }
}

