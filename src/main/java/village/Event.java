package village;

public enum Event {
    ARRIVE,
    LEAVE,
    SLEEP,
    VISIT
}
