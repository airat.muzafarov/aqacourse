package village;

public enum Role {
    PEASANT,
    WITCH,
    VAMPIRE,
    WEREWOLF
}
