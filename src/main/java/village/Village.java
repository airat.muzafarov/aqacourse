package village;

 /*
Должен быть экземпляр класса Village, в котором тем или иным способом хранятся данные жителях (до 50 штук)
Жители принадлежат к следующим классам
Peasant   Witch    Vampire  Werewolf

У каждого жителя деревни должно быть уникальное имя и фамилия.
История деревни состоит из записей о событиях.
История формируется циклом, перебирающим каждый день и генерирующим 1 событие.
Каждый день должно произойти одно из четырех событий:

Житель приезжает в деревню (добавляется в Village)
Житель уезжает из деревни (убирается из Village)
Житель идет в гости к другому жителю
Все спят. Ничего не происходит.

Встреча в гостях приводит к следующим последствиям
Любой житель (кроме Witch) встречается с Witch в пятницу 13-е или 31 октября.
Вызывается MurderException и житель исчезает из деревни.
Любой житель встречается с Vampire
Если житель Peasant или Witch 5% шанс, что житель становится Vampire
5% Вызывается MurderException и житель исчезает из деревни.
1% Вызывается DawnException и Vampire исчезает из деревни.
Любой житель встречается с Werewolf в полнолуние
Вызывается MurderException и житель исчезает из деревни.
В остальных случаях происходит просто чаепитие.
 */

import village.util.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

public class Village {
    List<Human> villagers;

    Random rnd = new Random();
    Calendar currentDate = new GregorianCalendar(1650, Calendar.JANUARY, 1);
    Calendar endDate = new GregorianCalendar(1750, Calendar.JANUARY, 1);
    int countDay = 0;

    public static void main(String[] args) throws IOException {
        Village village = new Village();
        village.runLifeCycle();
        HistoryReader reader = new HistoryReader();
        reader.writeToFile(Messenger.getHistory());
        reader.readFromFile(Paths.get("src/main/java/village/history/VillageHistory.txt"));
    }

    public void runLifeCycle() {
        villagers = HumanGenerator.getPopulation(50);
        while (currentDate.before(endDate)) {
            int bound;
            if (villagers.isEmpty()) {
                bound = 1;
            } else if (villagers.size() == 1) {
                bound = 3;
            } else {
                bound = 4;
            }
            int action = rnd.nextInt(bound);
            switch (action) {
                case 0:
                    newVillager();
                    break;
                case 1:
                    leaveVillage();
                    break;
                case 2:
                    Messenger.print(currentDate);
                    break;
                case 3:
                    int size = villagers.size();
                    try {
                        visits(villagers.get(rnd.nextInt(size)), villagers.get(rnd.nextInt(size)));
                    } catch (MurderException | DawnException e) {
                    }
                    break;
            }
            currentDate.add(Calendar.DATE, 1);
            countDay++;
        }
    }

    public void visits(Human vl1, Human vl2) throws MurderException, DawnException {
        if ((currentDate.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY && currentDate.get(Calendar.DAY_OF_MONTH) == 13)
                || (currentDate.get(Calendar.MONTH) == Calendar.OCTOBER && currentDate.get(Calendar.DAY_OF_MONTH) == 31)
                &&
                ((vl1.getRole().equals(Role.WITCH) && vl2.getRole().equals(Role.PEASANT)) ||
                        (vl1.getRole().equals(Role.PEASANT) && vl2.getRole().equals(Role.WITCH)))
        ) {
            Human victim = vl1.getRole().equals(Role.PEASANT) ? vl1 : vl2;
            Human killer = vl1.getRole().equals(Role.PEASANT) ? vl2 : vl1;
            throw new MurderException(killer, victim, villagers, currentDate);
        }
        if (vl1.getRole().equals(Role.VAMPIRE) || vl2.getRole().equals(Role.VAMPIRE)) {
            if ((vl1.getRole().equals(Role.PEASANT) || vl2.getRole().equals(Role.PEASANT)) ||
                    (vl1.getRole().equals(Role.WITCH) || vl2.getRole().equals(Role.WITCH))) {
                int rndCase = rnd.nextInt(100);
                if (rndCase < 30) {
                    Human vampire = vl1.getRole().equals(Role.VAMPIRE) ? vl1 : vl2;
                    throw new DawnException(vampire, villagers, currentDate);
                } else if (rndCase < 100) {
                    int rndMurderOrNot = rnd.nextInt(2);
                    if (rndMurderOrNot == 0) {
                        Human victim = vl1.getRole().equals(Role.PEASANT) ? vl1 : vl2;
                        Human killer = vl1.getRole().equals(Role.PEASANT) ? vl2 : vl1;
                        throw new MurderException(killer, victim, villagers, currentDate);
                    } else {
                        Human vl = !vl1.getRole().equals(Role.VAMPIRE) ? vl1 : vl2;
                        vl.setRole(Role.VAMPIRE);
                        villagers.set(villagers.indexOf(vl), vl);
                        Messenger.printMessage(vl, MessageType.BecameVampire, currentDate);
                        return;
                    }
                }
            }
        }
        if (countDay % 28 == 0 && (vl1.getRole().equals(Role.WEREWOLF) || (vl2.getRole().equals(Role.WEREWOLF)))) {
            Human victim = vl1.getRole().equals(Role.WEREWOLF) ? vl2 : vl1;
            Human killer = vl1.getRole().equals(Role.WEREWOLF) ? vl1 : vl2;
            throw new MurderException(killer, victim, villagers, currentDate);
        }
        drinkTea(vl1, vl2);
    }

    private void leaveVillage() {
        Human villager = villagers.remove(rnd.nextInt(villagers.size()));
        Messenger.printMessage(villager, MessageType.Leave, currentDate);
    }

    private void newVillager() {
        while (true) {
            Human villager = HumanGenerator.getPopulation(1).get(0);
            if (!villagers.contains(villager)) {
                villagers.add(villager);
                Messenger.printMessage(villager, MessageType.Appeared, currentDate);
                break;
            }
        }
    }

    private void drinkTea(Human vl1, Human vl2) {
        Messenger.printMessage(vl1, vl2, MessageType.Tea, currentDate);
    }
}

