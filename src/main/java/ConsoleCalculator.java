import org.apache.commons.lang3.StringUtils;

import java.util.Scanner;

public class ConsoleCalculator {

    public static void calculate() {
        try {
            String str = inputData();
            String[] arrayData = parseString(str);
            int a = Integer.parseInt(arrayData[0]);
            int b = Integer.parseInt(arrayData[2]);
            switch (arrayData[1]) {
                case ("+"):
                    System.out.println(a + b);
                    break;
                case ("-"):
                    System.out.println(a - b);
                    break;
                case ("/"):
                    double result = (double) a / b;
                    if (a % b == 0) {
                        System.out.println((int) result);
                    } else {
                        System.out.println(result);
                    }
                    break;
                case ("*"):
                    System.out.println(a * b);
                    break;
            }
        } catch (Exception e) {
            System.out.println("Неверный формат выражения. Допустимый формат: целое число" +
                    " математическиая операция(+, -, /, *)" +
                    " целое число");
        }
    }

    private static String inputData() {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите выражение");
        return input.nextLine().trim();
    }

    private static String[] parseString(String data) {
        String[] result = new String[3];
        int pos;
        if (data.startsWith("-")) {
            int buffer = StringUtils.indexOfAny(data.substring(1), '+', '-', '/', '*');
            pos = buffer + 1;
        } else {
            pos = StringUtils.indexOfAny(data, '+', '-', '/', '*');
        }
        result[0] = data.substring(0, pos).trim();
        result[1] = data.substring(pos, pos + 1).trim();
        result[2] = data.substring(pos + 1).trim();
        return result;
    }
}
