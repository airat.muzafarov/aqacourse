import cryptoGrapher.Decoder;
import cryptoGrapher.Encoder;
import cryptoGrapher.Generator;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите текст");
        String text = input.nextLine();
        System.out.println("Введите ключ");
        String encodedKey = input.nextLine();
        SecretKey generateKey = new Generator().generateKey(encodedKey);
        IvParameterSpec ivParameterSpec = new Generator().generateIvParameterSpec();
        Encoder encoder = new Encoder();
        byte[] enc = encoder.encode(text, generateKey, ivParameterSpec);
        String result = new Decoder().decode(enc, generateKey, ivParameterSpec);
        System.out.println(result);
    }
}
