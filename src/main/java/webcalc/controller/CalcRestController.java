package webcalc.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import webcalc.service.CalcService;

import java.util.Map;

@RestController
public class CalcRestController {
    CalcService service = new CalcService();

    @GetMapping(path="/multi/{number}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> getVillage(@PathVariable int number) {
        return service.multi(number);
    }

}
