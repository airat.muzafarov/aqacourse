package webcalc.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CalcService {

    public Map<String, String> multi(int number) {
        Map<String, String> map = new HashMap<>();
        for (int i =1; i < 5; i++) {
            int prod  = (int) Math.pow(number, i);
            map.put(String.valueOf(i), String.valueOf(prod));
        }
        return map;
    }
}
