package steam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import steam.pages.CategoryPage;
import steam.pages.GamePage;
import steam.pages.MainPage;
/*
Переходим на сайт стима.
Проверям на каком языке, если не на англ, то переключаем на него.
Переходим в категорию и выбираем Action
Выбираем первую игру на слайдбаре и запоминаем цену.
Заходим на страницу этой игры и сравниваем цену.
 */
public class SteamTest extends BaseTest {

    @Test
    public void checkGamePriceOnCategoryPageAndGamePage() {
        MainPage mainPage = new MainPage();
        if (!mainPage.isEngLang()) {
            mainPage.clickLangMenu();
            mainPage.selectEngLang();
        }
        mainPage.clickGenreTab();
        mainPage.clickGenreTab();
        mainPage.isNavigatorDisplayed();
        mainPage.clickActionGenre();
        CategoryPage categoryPage = new CategoryPage();
        categoryPage.isCategoryNameHeaderDisplayed();
        categoryPage.clickToFirstGameInSlide();
        String expectedPrice = categoryPage.getPriceOnCategoryPage();
        categoryPage.clickToGame();
        GamePage gamePage = new GamePage();
        Assertions.assertEquals(expectedPrice, gamePage.getGamePriceInGamePage());
    }


}
