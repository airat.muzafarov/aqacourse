package steam.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class GamePage {

    SelenideElement gamePriceInGamePage = $x("//div[@class ='game_area_purchase_game']//div[@data-price-final]");
    SelenideElement gameFreeToPlayInGamePage = $x("//*[@class = 'game_purchase_action']//*[@class='game_purchase_price price']");

    public String getGamePriceInGamePage() {
        if(gameFreeToPlayInGamePage.isDisplayed() && gameFreeToPlayInGamePage.getText().equals("Free To Play")) {
            return "0";
        }
        return gamePriceInGamePage.getAttribute("data-price-final");
    }
}
