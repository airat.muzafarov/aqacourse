package util;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class TestHelperTest {

    @Test
    void getRandomNumber() {
        assertTrue(TestHelper.getRandomNumber() < 2023);
        assertTrue(TestHelper.getRandomNumber() > 1969);
    }

    @Test
    void getRandomLong() {
        assertTrue(TestHelper.getRandomLong() > 0);
    }

    @Test
    void generateString() {
        String str = TestHelper.generateString(8);
        String[] arr = str.split(" ");
        assertEquals(26, str.length());
        assertEquals(3, arr.length);
    }

    @Test
    void readFile() {
        HashMap<String, String> result = new HashMap<>();
        try {
            result = TestHelper.readFile("src/test/java/resources/TestData.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertFalse(result.isEmpty());
        assertEquals(7, result.size());
    }

    @Test
    void dateFormatter() {
        String oldDateString = "1988-05-21";
        String[] arr = TestHelper.dateFormatter(oldDateString).split(" ");
        assertAll(
                () -> assertEquals("21", arr[0]),
                () -> assertEquals("мая", arr[1]),
                () -> assertEquals("1988", arr[2])
        );
    }

    @Test
    void stringFormatter() {
        assertAll(
                () -> assertEquals(0.0, TestHelper.stringFormatter("0")),
                () -> assertEquals(-1.0, TestHelper.stringFormatter("-1")),
                () -> assertEquals(1.0, TestHelper.stringFormatter("1")),
                () -> assertEquals(Double.NEGATIVE_INFINITY, TestHelper.stringFormatter("dsf"))
        );
    }
}