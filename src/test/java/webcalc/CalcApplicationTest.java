package webcalc;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CalcApplicationTest {

    @LocalServerPort
    private Integer port;
    private RequestSpecification request;

    @BeforeEach
    public void setup(){
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost" + port;
    }

    @Test
    public void getMulti() {
        Response response = request.get("/multi/4");
        assertEquals("4", response.jsonPath().get("1"));
        assertEquals("16", response.jsonPath().get("2"));
        assertEquals("64", response.jsonPath().get("3"));
        assertEquals("256", response.jsonPath().get("4"));
    }


}