package creditor;

import creditor.model.Client;
import creditor.model.LoanRequest;
import creditor.repository.LoanRequestRepository;
import creditor.steps.CreditorSteps;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CreditorRepoTest extends BaseTest {
    @Autowired
    private LoanRequestRepository repo;

    LoanRequest loanRequest = LoanRequest.builder()
            .loanAmount(5000)
            .client(Client.builder().age(20).name("Ivan").build())
            .build();

    @Test
    public void createLoan() {
        LoanRequest loan = repo.save(loanRequest);
        Integer id = loan.getId();
        ValidatableResponse response = CreditorSteps.getLoan(request, id);
        response.assertThat()
                .body("id", equalTo(loan.getId()))
                .body("loanAmount", equalTo(loan.getLoanAmount()))
                .body("client.name", equalTo(loan.getClient().getName()));
    }

    @Test
    public void getLoan() {
        LoanRequest loan = repo.save(loanRequest);
        Integer id = loan.getId();
        LoanRequest loanRepo = repo.findById(id).get();
        ValidatableResponse loanResponse = CreditorSteps.getLoan(request, id)
                .assertThat()
                .statusCode(200)
                .body("id", equalTo(loanRepo.getId()))
                .body("loanAmount", equalTo(loanRepo.getLoanAmount()))
                .body("client.name", equalTo(loanRepo.getClient().getName()));
    }

    @Test
    public void deleteLoan() {
        File file = new File("src/test/java/json/delete.json");
        CreditorSteps.createLoan(request, loanRequest)
                .statusCode(200);
        CreditorSteps.deleteLoan(request, 0)
                .assertThat()
                .body(matchesJsonSchema(file))
                .statusCode(200)
                .body("success", equalTo(true));
    }

    @Test
    public void getList() {
        List<LoanRequest> listRepo = repo.findAll();
        List<LoanRequest> list = CreditorSteps.getLoanList(request)
                .assertThat()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getList("");
        assertEquals(listRepo, list);
    }
}
