package creditor;


import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class BaseTest {
    @LocalServerPort
    private Integer port;

    public RequestSpecification request;

    @BeforeEach
    public void setup(){
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost:" + port;
    }

}
