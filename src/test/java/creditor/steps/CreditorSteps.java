package creditor.steps;

import creditor.model.LoanRequest;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public  class CreditorSteps {
    private static final String endpoint = "/api/creditor/";

    public static ValidatableResponse createLoan(RequestSpecification request, LoanRequest loanRequest){
        return request.contentType("application/json")
                .body(loanRequest)
                .post(endpoint + "save")
                .then();
    }

    public static ValidatableResponse getLoan(RequestSpecification request, Integer id){
        return request.contentType("application/json")
                .get(endpoint + id)
                .then();
    }

    public static ValidatableResponse deleteLoan(RequestSpecification request, Integer id){
        return request.contentType("application/json")
                .delete(endpoint + id)
                .then();
    }

    public static ValidatableResponse getLoanList(RequestSpecification request){
        return request.contentType("application/json")
                .get(endpoint)
                .then();
    }
}
