package creditor;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class CreditTest {
    Credit credit = new Credit();
    Human human = new Human("name", 20, 1900);
    Human falseHuman = new Human("name", 20, 50000);

    @Test
    public void getJson() {
        String expected = "[{\"name\":\"name\",\"age\":20,\"sum\":1900}]";
        String actual = credit.getJsonFromList(Arrays.asList(human));
        assertEquals(expected, actual);
    }

    @Test
    public void consoleOutputTrue() {
        PrintStream stream = mock(PrintStream.class);
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        System.setOut(stream);
        credit.checkHumansForCredit(credit.getJsonFromList(Arrays.asList(human)));
        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), "Кредит name выдан: true");
    }

    @Test
    public void consoleOutputFalse() {
        PrintStream stream = mock(PrintStream.class);
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        System.setOut(stream);
        credit.checkHumansForCredit(credit.getJsonFromList(Arrays.asList(falseHuman)));
        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), "Кредит name выдан: false");
    }

    @Test
    public void creditForHuman() {
        assertTrue(credit.checkHumanForCredit(human));
        assertFalse(credit.checkHumanForCredit(falseHuman));
    }
}