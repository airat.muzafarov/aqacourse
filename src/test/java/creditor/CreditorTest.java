package creditor;

import creditor.model.Client;
import creditor.model.LoanRequest;
import creditor.steps.CreditorSteps;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CreditorTest extends BaseTest {
    LoanRequest loanRequest = LoanRequest.builder()
            .loanAmount(5000)
            .client(Client.builder().age(20).name("Ivan").build())
            .build();

    @Test
    public void createLoan() {
        File file = new File("src/test/java/json/create.json");
        CreditorSteps.createLoan(request, loanRequest)
                .assertThat()
                .body(matchesJsonSchema(file))
                .statusCode(200);
    }

    @Test
    public void getLoan() {
        File file = new File("src/test/java/json/getLoan.json");
        CreditorSteps.createLoan(request, loanRequest)
                .statusCode(200);
        CreditorSteps.getLoan(request, 0)
                .assertThat()
                .body(matchesJsonSchema(file))
                .statusCode(200)
                .body("id", equalTo(0))
                .body("loanAmount", equalTo(5000))
                .body("client.name", equalTo("Ivan"));
    }

    @Test
    public void deleteLoan() {
        File file = new File("src/test/java/json/delete.json");
        CreditorSteps.createLoan(request, loanRequest)
                .statusCode(200);
        CreditorSteps.deleteLoan(request, 0)
                .assertThat()
                .body(matchesJsonSchema(file))
                .statusCode(200)
                .body("success", equalTo(true));
    }

    @Test
    public void getList() {
        File file = new File("src/test/java/json/getList.json");
        CreditorSteps.createLoan(request, loanRequest)
                .statusCode(200);
        CreditorSteps.createLoan(request, loanRequest)
                .statusCode(200);
        CreditorSteps.createLoan(request, loanRequest)
                .statusCode(200);
        List<LoanRequest> list = CreditorSteps.getLoanList(request)
                .assertThat()
                .body(matchesJsonSchema(file))
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getList("");
        assertEquals(20, list.get(0).getClient().getAge());
        assertEquals(3, list.size());
    }
}
