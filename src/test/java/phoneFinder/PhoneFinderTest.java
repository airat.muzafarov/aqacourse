package phoneFinder;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PhoneFinderTest {
    PhoneFinder phoneFinder = new PhoneFinder();

    @Test
    void getString() {
        String s = phoneFinder.getString(5000);
        assertEquals(5000, s.length());
    }
    @ParameterizedTest
    @ValueSource(strings = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"})
    void checkString(String value) {
        String s = phoneFinder.getString(5000);
        assertTrue(s.contains(value));
    }

    @Test
    void getPhoneNumbers() {
        String number = "81237777777";
        String falseNumber = "89172731647";
        List<String> result = phoneFinder.getPhoneNumbers(phoneFinder.getString(5000) + number + falseNumber);
        assertTrue(result.contains(number));
        assertFalse(result.contains(falseNumber));

    }
}